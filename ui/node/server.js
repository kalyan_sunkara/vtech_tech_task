const Koa = require('koa');
var app = new Koa();
const KoaBodyParser = require('koa-bodyparser');
const Cors = require('koa-cors');
app.use(Cors());
app.use(KoaBodyParser());
app.use(require('./routes.js'));
app.listen(3333
    , () => console.log('koa-app started and listening in 3333'));
