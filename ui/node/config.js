const { extend } = require('pg-extra');
const pg = extend(require('pg'));
const DataBaseUrl = 'postgres://postgres:postgresql@localhost:5432/gamification';
const pool = new pg.extra.Pool({ connectionString: DataBaseUrl });
module.exports = pool;