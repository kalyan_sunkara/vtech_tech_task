const Router = require('koa-router');
const Koa = require('koa');
const configurationHandler = require('./configuration-service.js');
const app = new Koa();
var router = new Router();
app.use(router.routes())
    .use(router.allowedMethods());
console.log("in routes")
router.get('/getall', configurationHandler.getall);
router.put('/edit', configurationHandler.edit);
router.delete('/delete/:id', configurationHandler.delete);
router.post('/registration', configurationHandler.studentRegistration);

module.exports = router.routes();

// hello all