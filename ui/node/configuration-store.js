const pool = require("./config.js");
const { sql } = require('pg-extra');


var configurationStore =
{
    updateConfiguration: async function (value, key) {
       console.log('in update config');
       
        let db = await pool.one(sql`update configuration set value =${value} 
                                      where  key=${key} returning * `)
        return db

    },
    getConfigurationDetails: async function () {

        let db = await pool.many(sql`select * from configuration `)   
        return db

    }
}
module.exports = configurationStore;
