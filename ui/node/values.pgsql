select * from topic;

select * from configuration;

select * from topic_status;

select * from level_status;

select * from level;

select * from program;

select * from program_status;
truncate table program;

truncate table level;

truncate table topic;

delete from program;

delete from level;

delete from topic;

insert into program(program_id,program_name)
values('P1','Full Stack'),
('P2','Selenium');

insert into level(level_id,program_id,level_name)
values('L1','P1','level_1'),
('L2','P1','level_2'),
('L3','P1','level_3'),
('L4','P1','level_4'),
('L5','P1','level_5'),
('L6','P2','level_6'),
('L7','P2','level_7'),
('L8','P2','level_8'),
('L9','P2','level_9'),
('L10','P2','level_10');

insert into topic(topic_id,level_id,topic_name)
values('T1','L1','Java'),
('T2','L1','Html'),
('T3','L2','Css'),
('T4','L2','Bootstrap'),
('T5','L3','JavaScript'),
('T6','L3','SQL'),
('T7','L4','Swagger'),
('T8','L4','Postman'),
('T9','L5','NodeJs'),
('T10','L5','React'),
('T11','L6','SeleniumDesign'),
('T12','L6','WebdriverArchitecture'),
('T13','L7','WebdriverAPI'),
('T14','L7','LoadTesting'),
('T15','L8','DatabaseTesting'),
('T16','L8','PerformanceTesting'),
('T17','L9','FrameworkDesign'),
('T18','L9','Jenkins'),
('T19','L10','PageObject'),
('T20','L10','DataDriven');



















