import React, { Component } from 'react'
import { Menu} from 'antd';
// import Programs from './Programs.js';
import './game.css';
import Axios from 'axios';
import { withRouter } from 'react-router-dom';
import Programs from './Programs';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.handleNavigation = this.handleNavigation.bind(this);
    }
    handleNavigation(e) {
        console.log('this.props.history', this.props.history);
        
        if (e.key === "1") {
            console.log('in key 1');
            this.props.history.push({
                pathname: "/User",
                state: {
                    user_id: cookies.get("userId"),
                    coins: cookies.get("totalCoins")
                    // badge_name: response.data[0].badge_name
                }
            })
        }
        if (e.key === "2") {
            console.log('in key 2');
            this.props.history.push("/leaderboard")

 
        }
        
    }
  
  render() {
    return (
        <div className="vl1">
            <br />
            <br />
            <br />
            <br />
            <Menu
                mode="vertical"
            onClick={this.handleNavigation}>
                <Menu.Item className="vl" key="1"><h3
                    style={{ fontSize: "28px", color: "#2864FF" }}
                >Programs
            </h3></Menu.Item>    
                <br />
                <br />
                <Menu.Item
                    className="vl"
                    key="2"
                ><h3
                    style={{ fontSize: "28px", color: "#2864FF" }}
                >Leader Board
            </h3></Menu.Item>      
            </Menu>
            </div>
        )
    }
}
export default withRouter(Navigation);