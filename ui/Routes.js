import React, { Component } from 'react';
import { Route, BrowserRouter as Router, Switch,withRouter } from 'react-router-dom'
// import Programs from './Programs.js'
import App from './App'
import LeaderBoard from './Edit.js';

class Routes extends Component {
    render() {
        return (

            <Router>
                <Switch>
                    <Route exact path ="/" component={App} />
                    <Route path ="/edit" component={LeaderBoard} />
                </Switch>
            </Router>
        );
    }
}

export default Routes;
// export default withRouter(Routes);

