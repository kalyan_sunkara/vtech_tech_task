import React, { Component } from 'react';
import { Input, Row, Col, Button, Avatar, Card, Modal, message } from 'antd';
import { Link } from 'react-router-dom';
const axios = require('axios');

class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // id:this.props.location.state.id,
            index: this.props.location.index,
            firstname: this.props.location.state.Fullname,
            Email: this.props.location.state.email,
            phonenumber: this.props.location.state.phonenumber,
            exist: []
        }
        this.onChange = this.onChange.bind(this)
    }
    onChange(e, value) {
        console.log(e.target.value);

        this.setState({ [value]: e.target.value })
    }
    render() {
        return (
            <div>
                <Card title="Contact us" className="padding"
                    style={{ width: "100%", borderColor: "#010707" }}>

                    <Row>
                        <Col xs={1} sm={1} md={1} lg={1}>
                        </Col>
                        <Col xs={2} sm={2} md={2} lg={2}  >
                            <h4 >Full name</h4>
                        </Col>
                        <Col xs={4} sm={4} md={4} lg={4}  >
                            < Input
                                value={this.state.firstname}
                                style={{ width: 190 }}
                                onChange={(e) => this.onChange(e, "firstname")}
                                placeholder="Enter your name" />
                        </Col>
                        <Col xs={2} sm={2} md={2} lg={2}  >
                            <h4 >Email :</h4>
                        </Col>

                        <Col xs={4} sm={4} md={4} lg={4}  >
                            < Input
                                value={this.state.Email}
                                style={{ width: 190 }}
                                onChange={(e) => this.onChange(e, "Email")}

                                placeholder="Enter your name" />
                        </Col>
                        <Col xs={2} sm={2} md={2} lg={2}  >
                            <h4 >Mobile</h4>
                        </Col>
                        <Col xs={3} sm={3} md={3} lg={3}  >
                            < Input
                                value={this.state.phonenumber}
                                style={{ width: 190 }}
                                onChange={(e) => this.onChange(e, "phonenumber")}
                                placeholder="Enter your name" />
                        </Col>
                        <Col xs={1} sm={1} md={1} lg={1}  >
                        </Col>
                        <Col xs={3} sm={3} md={3} lg={3}  >
                            <Button onClick={() => {
                                let body = {
                                    Fullname: this.state.firstname,
                                    email: this.state.Email,
                                    phonenumber: this.state.phonenumber,
                                    index: this.state.index
                                }
                                axios.put('http://localhost:3333/edit', body).then(res => {
                                    console.log(res.data);
                                    this.props.history.push("/")
                                })
                            }}>submit</Button>
                        </Col>
                    </Row>
                </Card>
            </div>
        );
    }
}
export default Edit;