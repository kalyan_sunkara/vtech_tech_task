import React, { Component } from 'react';
import './Table.css'
const axios = require('axios');
// import Modal from 'react-modal'
import _ from 'lodash';
import { Input, Row, Col, Button, Avatar, Card, Modal, message } from 'antd';
const { confirm } = Modal;
class App extends Component {
    constructor(props) {
        super(props);
        this.id = _.uniqueId("");
        this.state = {
            firstname: "",
            Email: "",
            phonenumber: "",
            exist: []
        }
        this.edit = this.edit.bind(this)
        this.onChange = this.onChange.bind(this)
        this.Register = this.Register.bind(this)
        this.renderTableHeader = this.renderTableHeader.bind(this)
        this.renderTableData = this.renderTableData.bind(this)
        this.showConfirm = this.showConfirm.bind(this)
    }
    showConfirm(index, value) {
        confirm({
            title: 'Do you Want to delete these items?',
            content: 'Some descriptions',
            onOk: () => {
                console.log(index);

                axios.delete('http://localhost:3333/delete/' + index).then(res => {
                    console.log(res.data);
                    this.setState({ exist: res.data })
                })
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }
    onChange(e, value) {
        console.log(e.target.value);

        this.setState({ [value]: e.target.value })
    }
    Register() {
        let body = {
            id: this.id,
            Fullname: this.state.firstname,
            email: this.state.Email,
            phonenumber: this.state.phonenumber
        }
        axios.post('http://localhost:3333/registration', body).then(response => {
            console.log("post response", response);


            this.setState({ exist: response.data }, () => { });
            this.setState({
                firstname: "",
                Email: "",
                phonenumber: ""
            })
        });

    }
    edit(item, value, operation) {
        if (operation == "edit") {
            this.props.history.push({ pathname: '/edit', state: item, index: value })
        }


    }
    renderTableData() {
        let array = []
        this.state.exist.map((item, index) => {
            array.push(<tr>
                <td>{index}</td>
                <td>{item.Fullname}</td>
                <td>{item.email}</td>
                <td>{item.phonenumber}</td>
                <td>

                    <div style={{ display: "flex" }}>
                        <p style={{ paddingLeft: "10px", marginTop: "18px" }}>
                            <button className="editbtn" onClick={() => this.edit(item, index, "edit")}>
                                edit
                                    </button>
                        </p>
                        <p style={{ paddingLeft: "10px", marginTop: "18px" }}>
                            <button className="editbtn" onClick={() => this.showConfirm(index, "delete")}>
                                delete
                                        </button>
                        </p>
                    </div>
                </td>
            </tr>)
        })
        return array
    }
    closeModal() {
        this.setState({ modalIsOpen: false });
    }
    componentDidMount() {
        console.log("componentDidMount");
        axios.get('http://localhost:3333/getall').then(response => {
            console.log("response", response);

            this.setState({ exist: response.data })

        }).catch((err) => {
            console.log("errrrrr", err)
        });
    }
    renderTableHeader() {
        let array = []
        let head = ["NO", "Fullname", "email", "phone", "Action"]
        head.map((item, index) => {
            array.push(<th style={{ padding: "5px" }}>
                {item}
            </th>)
        })
        return array
    }
    render() {

        return (
            <div>
                <Card title="Contact us" className="padding"
                    style={{ width: "100%", borderColor: "#010707" }}>

                    <Row>
                        <Col xs={1} sm={1} md={1} lg={1}>
                        </Col>
                        <Col xs={2} sm={2} md={2} lg={2}  >
                            <h4 >Full name</h4>
                        </Col>
                        <Col xs={4} sm={4} md={4} lg={4}  >
                            < Input
                                style={{ width: 190 }}
                                onChange={(e) => this.onChange(e, "firstname")}
                                value={this.state.firstname}
                                placeholder="Enter your name" />
                        </Col>
                        <Col xs={2} sm={2} md={2} lg={2}  >
                            <h4 >Email :</h4>
                        </Col>

                        <Col xs={4} sm={4} md={4} lg={4}  >
                            < Input
                                style={{ width: 190 }}
                                onChange={(e) => this.onChange(e, "Email")}
                                value={this.state.Email}
                                placeholder="Enter your name" />
                        </Col>
                        <Col xs={2} sm={2} md={2} lg={2}  >
                            <h4 >Mobile</h4>
                        </Col>
                        <Col xs={3} sm={3} md={3} lg={3}  >
                            < Input
                                style={{ width: 190 }}
                                onChange={(e) => this.onChange(e, "phonenumber")}
                                value={this.state.phonenumber}
                                placeholder="Enter your name" />
                        </Col>
                        <Col xs={1} sm={1} md={1} lg={1}  >
                        </Col>
                        <Col xs={3} sm={3} md={3} lg={3}  >
                            <Button type="primary"
                                style={{ background: "none", color: "black" }}
                                onClick={this.Register}>
                                submit
                                </Button>
                        </Col>
                    </Row>
                </Card>
                <Card title="Contact us list">
                    <table id="students">
                        <tbody>
                            <tr id="title" >{this.renderTableHeader()}</tr>
                            {
                                this.renderTableData()
                            }
                        </tbody>
                    </table>
                </Card>

            </div>
        );
    }
}

export default App;